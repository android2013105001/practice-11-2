package resolution.example6.zzeulki.practice112;

import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MainActivity extends AppCompatActivity {

    private GLSurfaceView glSurface;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        glSurface = new GLSurfaceView(this);
        glSurface.setRenderer(new MyRenderer());
        setContentView(glSurface);
    }

    @Override
    protected void onResume() {
        super.onResume();
        glSurface.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
        glSurface.onPause();
    }

}

class MyRenderer implements Renderer {
    private Tri triangle;
    private resolution.example6.zzeulki.practice112.Rect rectangle;

    float rotateTri = 0.0f;
    float rotateRect = 0.0f;

    public MyRenderer() {
        triangle = new Tri();
        rectangle = new Rect();
    }
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        gl.glShadeModel(GL10.GL_SMOOTH); //Enable Smooth Shading
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f); //Black Background
        gl.glClearDepthf(1.0f); //Depth Buffer Setup
        gl.glEnable(GL10.GL_DEPTH_TEST); //Enables Depth Testing
        gl.glDepthFunc(GL10.GL_LEQUAL); //The Type Of Depth Testing To Do
//Really Nice Perspective Calculations
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        if (height == 0) { //Prevent A Divide By Zero By
            height = 1; //Making Height Equal One
        }
        gl.glViewport(0, 0, width, height); //Reset The Current Viewport
        gl.glMatrixMode(GL10.GL_PROJECTION); //Select The Projection Matrix
        gl.glLoadIdentity(); //Reset The Projection Matrix
//Calculate The Aspect Ratio Of The Window
        GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f, 100.0f);
        gl.glMatrixMode(GL10.GL_MODELVIEW); //Select The Modelview Matrix
        gl.glLoadIdentity(); //Reset The Modelview Matrix
    }

    @Override
    public void onDrawFrame(GL10 gl) {


        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();
        gl.glTranslatef(0.0f, 0.0f, -6.0f);
        gl.glRotatef(rotateRect, 0.0f, 0.0f, 1.0f);
        gl.glTranslatef(0.0f, -1.2f, -0.0f);
        rectangle.draw(gl);

        //gl.glLoadIdentity();
        gl.glTranslatef(0.0f, 2.5f, 0.0f);
        //gl.glRotatef(rotateRect, 0.0f, 0.0f, 1.0f);
        triangle.draw(gl);

        rotateTri = 1.0f;
        rotateRect += 1.0f;
    }
}