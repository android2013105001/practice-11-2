package resolution.example6.zzeulki.practice112;

/**
 * Created by Zzeulki on 16. 11. 14..
 */
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Rect extends Obj {
    private static float vertices[] = {
            -1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
            -1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f
    };
    private static float colors[] = {
            1.0f, 0.0f, 0.0f, 1.0f, //Set The Color To Red, last value 100% luminance
            0.0f, 1.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 1.0f
    };
    public Rect() {
        ByteBuffer byteBuf = ByteBuffer.allocateDirect(vertices.length * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        vertexBuffer = byteBuf.asFloatBuffer();
        vertexBuffer.put(vertices);
        vertexBuffer.position(0);
        byteBuf = ByteBuffer.allocateDirect(colors.length * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        colorBuffer = byteBuf.asFloatBuffer();
        colorBuffer.put(colors);
        colorBuffer.position(0);
        vertexLength = vertices.length / 3;
        colorVertex = true;
    }
}